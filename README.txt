Commerce License Access Control
===============================

The Commerce License Access Control module uses ACL and Commerce License to allow sites to sell content with Drupal Commerce.

A license can grant view, update and/or delete access to a specific node with priorities being handled by ACL.
